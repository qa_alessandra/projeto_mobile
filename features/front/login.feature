#language: pt

Funcionalidade: Login

@login_feliz
Cenario: Usuário logado

    Dado que acesso a tela de Login
    Quando realizar login com "ale.teste@qa.com.br" e "teste123"
    Então devo ver a tela minha conta

@login_falied
Esquema do Cenario: Tentativa de login

    Dado que acesso a tela de Login
    Quando realizar login com <email> e <senha>
    Então devo ver <mensagem> como popup


    Exemplos:
    |email                      |senha     |mensagem|
    |"ale.teste@qa.com.br"      |"123teste"|"Usuário e/ou senha inválidos."    |
    |"ale.emailerrado@qa.com.br"|"teste123"|"Usuário e/ou senha inválidos."    |
    |""                         |"teste123"|"Todos os campos são obrigatórios."|
    |"ale.emailerrado@qa.com.br"|""        |"Todos os campos são obrigatórios."|

  
@login_tentativas
Cenario: Login na 3a tentativa

    Dado que acesso a tela de Login
    Quando logo sem sucesso 2 vezes
    E faço login na última tentativa
    Então devo ver a tela minha conta


   







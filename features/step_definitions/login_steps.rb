Dado('que acesso a tela de Login') do
    @screen.home.botao_logar_home
end
      
Quando('realizar login com {string} e {string}') do |email, senha|        
    @screen.login.fazer_login(email, senha)
end

Quando('logo sem sucesso {int} vezes') do |tentativa|   

    tentativa.times do
        steps %(
            Quando realizar login com "ale.teste@qa.com.br" e "123teste"
        )
        popup = find_element(id: "android:id/message")
        expect(@screen.login.popup.displayed?).to be true
        back
    end

end  

Quando('faço login na última tentativa') do
    steps %(
        Quando realizar login com "ale.teste@qa.com.br" e "teste123"
    ) 
end
                                                                                                                                                        
      
Então('devo ver a tela minha conta') do
    @screen.home.botao_logar_home
    expect(@screen.conta.minha_conta.displayed?).to be true
end

Então('devo ver {string} como popup') do |mensagem_esperada|
    expect(@screen.login.popup.text).to eql mensagem_esperada
end
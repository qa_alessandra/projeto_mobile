class Home
  def botao_logar_home
    find_element(id: 'qaninja.com.pixel:id/accountButt').click
  end
end

class Login
  def fazer_login(email, senha)
    find_element(id: 'qaninja.com.pixel:id/usernameTxt').send_keys(email)
    find_element(id: 'qaninja.com.pixel:id/passwordTxt').send_keys(senha)
    find_element(id: 'qaninja.com.pixel:id/loginButt').click
  end

  def popup
    find_element(id: 'android:id/message')
  end
end

class Conta
  def minha_conta
    find_element(id: 'qaninja.com.pixel:id/account')
  end
end
